package arash.app.idtmessagingtest;

import android.content.Context;
import android.test.InstrumentationTestCase;
import android.test.mock.MockContext;

import org.junit.Test;

/**
 * Created by arash on 8/31/16.
 */
public class IDTUnitTest extends InstrumentationTestCase {

    Context context;

    public void setUp() throws Exception {
        super.setUp();

        context = new MockContext();

        assertNotNull(context);

    }

    @Test
    public void testURLCreation() {
        context = new MockContext();
        String actual = context.getString(R.string.image1);
        // expected value is https://upload.wikimedia.org/wikipedia/commons/a/a5/Star_Trek_William_Shatner.JPG
        String expected = "wrong variable!";
        assertNull(actual);
        assertNotSame(actual,expected);
    }
}
