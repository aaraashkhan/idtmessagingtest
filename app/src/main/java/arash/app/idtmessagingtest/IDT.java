package arash.app.idtmessagingtest;

import android.app.Application;

import com.karumi.dexter.Dexter;

import arash.app.idtmessagingtest.Model.ImageService;

/**
 * Created by arash on 8/31/16.
 */
public class IDT extends Application {
    public static ImageService service;

    @Override
    public void onCreate() {
        super.onCreate();
        Dexter.initialize(this);
    }
}
