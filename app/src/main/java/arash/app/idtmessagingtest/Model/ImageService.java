package arash.app.idtmessagingtest.Model;

import android.app.Service;
import android.content.Intent;
import android.database.Observable;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.ImageView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.apache.http.Header;

import java.io.File;
import java.io.FileOutputStream;

import arash.app.idtmessagingtest.Entity.Image;
import arash.app.idtmessagingtest.IDT;
import rx.Observer;

public class ImageService extends Service {

    private IBinder binder = new ImageServiceBinder();

    public class ImageServiceBinder extends Binder {
        public ImageServiceBinder getService() {
            return ImageServiceBinder.this;
        }
    }

    /**
     * I usually don't start services as START_STICKY
     * unless it is required to service being always in background
     *
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_NOT_STICKY;
    }

    public ImageService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        IDT.service = this;
        return binder;
    }

    public void fetchImage(Image image, final Observer<Bitmap> imageResult){
        new ImageManager().fetchImage(image,imageResult);
    }

}
