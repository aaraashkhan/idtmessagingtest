package arash.app.idtmessagingtest.Model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import java.io.File;
import java.io.FileOutputStream;

import arash.app.idtmessagingtest.Entity.Image;
import rx.Observer;

/**
 * Created by arash on 8/31/16.
 */
public class ImageManager {

    private String fileName;

    /**
     *This function return image from URL or from cache if availble!
     * I added reading from disk just in case! :)
     *
     * @param image
     * @param imageResult
     *
     * @exception java.io.FileNotFoundException
     * @exception OutOfMemoryError
     */
    public void fetchImage(Image image, final Observer<Bitmap> imageResult){
        AsyncHttpClient client = new AsyncHttpClient();

        int lastIndex = image.getImageUrl().length() - image.getImageUrl().replace("/", "").length();
        fileName = image.getImageUrl().split("/")[lastIndex];//We assign image url as name so the image file is unique
        File photo=new File(Environment.getExternalStorageDirectory(), fileName);

        //If image is already downloaded then don't download it again
        if (photo.exists()) {
            Bitmap bitmap = BitmapFactory.decodeFile(photo.getAbsolutePath());

            if (bitmap != null) {
                rotate(180, bitmap,imageResult);
            }else{
                rx.Observable.just(bitmap).subscribe(imageResult);
            }
        }else{
            client.get(image.getImageUrl(), new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Log.v("IDT_","Image successfully fetched!");
                    Bitmap bitmapImage = BitmapFactory.decodeByteArray(responseBody, 0, responseBody.length);
                    if (responseBody != null && responseBody.length > 0 && statusCode == 200){
                        new SaveImage().execute(responseBody);
                    }
                    if (bitmapImage != null) {
                        rotate(180, bitmapImage,imageResult);
                    }else{
                        rx.Observable.just(bitmapImage).subscribe(imageResult);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.v("IDT_","Image fetching failed!");
                }
            });
        }

    }

    /**
     *
     * @param angle
     * @param bitmap
     * @param imageResultObserver
     *
     * Rotating Image Via a matrix(Because you asked the "actual image should be rotated")
     */
    private void rotate(int angle, Bitmap bitmap,Observer<Bitmap> imageResultObserver) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);

        Bitmap scaledBitmap = Bitmap.
                createScaledBitmap(bitmap,bitmap.getWidth(),bitmap.getHeight(),true);

        Bitmap rotatedBitmap = Bitmap.
                createBitmap(scaledBitmap , 0, 0, scaledBitmap .getWidth(), scaledBitmap .getHeight(), matrix, true);
        rx.Observable.just(rotatedBitmap).subscribe(imageResultObserver);
    }

    /**
     * Saving image via AsyncTask in Order to not blocking UI Thread
     */
    class SaveImage extends AsyncTask<byte[], String, String> {
        @Override
        protected String doInBackground(byte[]... imageBytes) {
            File photo=new File(Environment.getExternalStorageDirectory(), fileName);

            if (photo.exists()) {
                photo.delete();
            }

            try {
                FileOutputStream fos=new FileOutputStream(photo.getPath());

                fos.write(imageBytes[0]);
                fos.close();
            }
            catch (java.io.IOException e) {
                Log.e("IDT_", e.toString());
            }

            return(null);
        }
    }
}
