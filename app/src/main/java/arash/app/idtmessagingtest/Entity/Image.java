package arash.app.idtmessagingtest.Entity;

/**
 * Created by arash on 8/31/16.
 */
public class Image {

    public Image(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
