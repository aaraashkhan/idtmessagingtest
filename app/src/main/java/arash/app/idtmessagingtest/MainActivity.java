package arash.app.idtmessagingtest;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONArray;
import org.json.JSONException;

import arash.app.idtmessagingtest.Entity.Image;
import arash.app.idtmessagingtest.Model.ImageService;
import rx.Observer;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView urlTextView;
    private EditText urlEditText;
    private Button   submitButton;
    private ImageView imageView;
    private ProgressBar pbHeaderProgress;
    private ImageService.ImageServiceBinder imageServiceBinder;
    private ImageService imageService;
    private Observer<Bitmap> imageObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        urlTextView = (TextView) findViewById(R.id.urlTextView);
        urlEditText = (EditText) findViewById(R.id.urlEditText);
        submitButton = (Button) findViewById(R.id.submitButton);
        imageView = (ImageView) findViewById(R.id.imageView);
        pbHeaderProgress = (ProgressBar) findViewById(R.id.pbHeaderProgress);

        submitButton.setOnClickListener(this);
        urlTextView.setOnClickListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "ImageView Reseted!Enter an url", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                imageView.setImageDrawable(null);
                urlEditText.setText("");
                hideProgressBar();
            }
        });

        Dexter.checkPermission(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                Log.v("IDT_", "permission granted");
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                Log.v("IDT_", "permission denied");
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                Log.v("IDT_", "permission rational should be shown");
            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        Intent playerIntent = new Intent(getApplicationContext(), ImageService.class);
        bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        startService(new Intent(this, ImageService.class));

        imageObserver = new Observer<Bitmap>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Bitmap bitmap) {
                if (bitmap != null){
                    imageView.setImageDrawable(null);
                    imageView.setImageBitmap(bitmap);
                }else{
                    Toast.makeText(getApplicationContext(),"An Error Occured!",Toast.LENGTH_LONG).show();
                }
                hideProgressBar();
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ImageService.ImageServiceBinder binder = (ImageService.ImageServiceBinder) service;
            //get service
            imageServiceBinder = binder.getService();
            imageService = IDT.service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    @Override
    public void onClick(View view) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(urlEditText.getWindowToken(), 0);

        switch (view.getId()){

            case R.id.submitButton:
                if (urlEditText.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please enter an url!",Toast.LENGTH_LONG).show();
                    return;
                }
                if (imageService!= null){
                    imageService.fetchImage(new Image(urlEditText.getText().toString()),imageObserver);
                    showProgressBar();
                }else {
                    Toast.makeText(getApplicationContext(),"Uh oh!Something went wrong!",Toast.LENGTH_LONG).show();
                }

                break;

            case R.id.urlTextView:
                loadSampleURLs(view);
                break;
        }
    }

    private void showProgressBar(){
        submitButton.setVisibility(View.GONE);
        pbHeaderProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar(){
        submitButton.setVisibility(View.VISIBLE);
        pbHeaderProgress.setVisibility(View.GONE);
    }

    private void loadSampleURLs(View view) {
        final PopupMenu imageLinkPopup = new PopupMenu(this, view, Gravity.RIGHT);

        Menu imageLinkMenu = imageLinkPopup.getMenu();
        imageLinkMenu.add(1, 0, 1, getString(R.string.image1)).setChecked(false);
        imageLinkMenu.add(2, 1, 2, getString(R.string.image2)).setChecked(false);

        imageLinkMenu.setGroupCheckable(1, true, true);
        imageLinkPopup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                submitButton.setVisibility(View.GONE);
                pbHeaderProgress.setVisibility(View.VISIBLE);
                switch (item.getItemId()) {
                    case 0:
                        if (imageService != null){
                            imageService.fetchImage(new Image(getString(R.string.image1)),imageObserver);
                            showProgressBar();
                        }else {
                            Toast.makeText(getApplicationContext(),"Uh oh!Something went wrong!",Toast.LENGTH_LONG).show();
                        }

                        break;
                    case 1:
                        if (imageService != null){
                            imageService.fetchImage(new Image(getString(R.string.image2)),imageObserver);
                            showProgressBar();
                        }else {
                            Toast.makeText(getApplicationContext(),"Uh oh!Something went wrong!",Toast.LENGTH_LONG).show();
                        }
                        break;
                }
                imageLinkPopup.dismiss();
                return true;
            }
        });
        imageLinkPopup.show();

    }
}
